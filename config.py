import os
from dotenv import load_dotenv

load_dotenv()


class Config:
    TG_TOKEN = os.environ.get("TELEGRAM_BOT_API_TOKEN")
    APP_URL = os.environ.get("APP_URL")
    APP_PORT = os.environ.get("PORT", 5000)
    PROXY_URL = os.environ.get("PROXY_URL")
    MONGODB_URI = os.environ.get("MONGODB_URI")
    MONGODB_DATABASE = os.environ.get("MONGODB_URI").split('/')[-1]


