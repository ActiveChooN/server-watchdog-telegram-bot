import re
from urllib.parse import urlparse
from datetime import datetime
from flask import Flask, render_template_string
from teleflask import Teleflask
from pytgbot.api_types.sendable.reply_markup import InlineKeyboardMarkup, InlineKeyboardButton
from teleflask.messages import HTMLMessage, PlainMessage
from pymongo import MongoClient
import logging
from config import Config
import templates

# Enable logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# logging.add_colored_handler(level=logging.INFO)

# Creating flask application
app = Flask(__name__)
app.config.from_object(Config)

# Adding telegram bot
bot = Teleflask(Config.TG_TOKEN, hostname=Config.APP_URL, hookpath="/{}".format(Config.TG_TOKEN))
bot.init_app(app)

# Establish connection to MongoDB
client = MongoClient(Config.MONGODB_URI)
db = client[Config.MONGODB_DATABASE]

# Selecting collections
users = db["users"]
servers = db["servers"]


@bot.command("start")
def start_command(update, text):
    if not users.count_documents({"chat_id": update.message.chat.id}):
        try:
            users.insert_one({"chat_id": update.message.chat.id})
        except:
            bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                             messages=HTMLMessage(render_template_string(templates.unexpected_error)))
        finally:
            bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                             messages=HTMLMessage(render_template_string(templates.welcome)))
    else:
        bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                         messages=HTMLMessage(render_template_string(templates.already_started)))


@bot.command("register")
def register_command(update, text):

    if text is None or len(text.split(' ')) < 2:
        bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                         messages=HTMLMessage(render_template_string(templates.wrong_request)))
        return
    args = text.split(' ')
    url = args[0]
    alias = " ".join(args[1:])
    if re.match(r"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$", url) is None:
        bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                         messages=HTMLMessage(render_template_string(templates.wrong_url)))
    elif not len(alias):
        bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                         messages=HTMLMessage(render_template_string(templates.empty_alias)))
    elif servers.count_documents({"chat_id": update.message.chat.id}) > 10:
        bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                         messages=HTMLMessage(render_template_string(templates.too_many_servers_registered)))
    elif servers.count_documents({"url": url, "chat_id": update.message.chat.id}):
        bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                         messages=HTMLMessage(render_template_string(templates.server_already_exists)))
    else:
        try:
            servers.insert_one({"chat_id": update.message.chat.id, "alias": alias,
                                "url": urlparse(url, "http").geturl().replace('http:///', 'http://'),
                                "last_offline": datetime.utcnow(), "last_online": datetime.utcnow()})
        except:
            bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                             messages=HTMLMessage(render_template_string(templates.unexpected_error)))
        finally:
            bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                             messages=HTMLMessage(render_template_string(templates.server_registered, alias=alias)))


@bot.command("test_inline")
def test_inline_command(update, text):
    buttons = [[
        InlineKeyboardButton(text="Test link", url="http://google.com/")
    ]]
    keyboard = InlineKeyboardMarkup(buttons)
    bot.send_message(reply_to=update.message.chat.id, reply_id=None,
                     messages=PlainMessage("Test.", reply_markup=keyboard))


if __name__ == "__main__":
    app.run("0.0.0.0", Config.APP_PORT, debug=True)
