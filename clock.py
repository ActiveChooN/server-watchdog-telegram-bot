import requests
from datetime import datetime
from apscheduler.schedulers.blocking import BlockingScheduler
from teleflask.messages import HTMLMessage
from flask import render_template_string
import templates
from app import bot, servers, app

scheduler = BlockingScheduler()


@scheduler.scheduled_job('interval', id='check_servers', minutes=15, misfire_grace_time=900)
def check_servers_job():
    with app.app_context():
        for server in servers.find():
            try:
                r = requests.get(server["url"])
            except requests.exceptions.ConnectionError:
                r = None
            if r is None or r.status_code != 200:
                if server["last_online"] > server["last_offline"]:
                    bot.send_message(reply_to=server["chat_id"], reply_id=None,
                                     messages=HTMLMessage(render_template_string(templates.server_is_down,
                                                                                 alias=server["alias"])))
                servers.update_one({"_id": server["_id"]}, {"$set": {"last_offline": datetime.utcnow()}})
            else:
                if r.elapsed.total_seconds() > app.config["REQUESTS_MAX_TIME"]:
                    bot.send_message(reply_to=server["chat_id"], reply_id=None,
                                     messages=HTMLMessage(render_template_string(templates.server_is_down,
                                                                                 alias=server["alias"],
                                                                                 time=r.elapsed.total_seconds())))
                servers.update_one({"_id": server["_id"]}, {"$set": {"last_online": datetime.utcnow()}})


if __name__ == "__main__":
    scheduler.start()
